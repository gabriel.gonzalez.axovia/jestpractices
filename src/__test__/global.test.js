const text = "Hola Mundo";
const frutas = ['manzana', 'melon','platano'];
test('Debe contener un texto',()=>{
  expect(text).toMatch(/Mundo/);
});

test('Tenemos una banana',()=>{
  expect(frutas).toContain('platano');
})

test('Mayor que',()=>{
  expect(10).toBeGreaterThan(9)
})

test('Verdadero',()=>{
  expect(true).toBeTruthy()
})

const reverseString = (str, callback) =>{
  callback(str.split("").reverse().join(""))
};

test('Probando un callback',()=>{
  reverseString('hola',(str)=>{
    expect(str).toBe('aloh')
  })
})

const reverseString2 = (str) =>{
  return new Promise((resolve,reject)=>{
    if(!str){
      reject(Error('Error'))
    }
    else{
      resolve(str.split("").reverse().join(""))
    }
  })
}

test('Probar una promesa',()=>{
  return reverseString2('hola')
  .then(string =>{
    expect(string).toBe('aloh')
  })
})


test('Probar async/await', async()=>{
  const string = await reverseString2('hola')
  expect(string).toBe('aloh')
})

// afterEach(()=> console.log("Despues de cada prueba"))
//
// afterAll(()=>console.log("Fin de pruebas"))
//
// beforeEach(()=>console.log('Antes de la prueba'))
// beforeAll(()=>console.log('Iniciando pruebas'))
